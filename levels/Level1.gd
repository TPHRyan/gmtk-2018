extends "res://engine/HintLevel.gd"

func _ready():
	$Shell.set_header(ASCII.generate_level_header("LEVEL ONE"))
	add_hint("Try clicking things. Specific words, perhaps.")
	add_hint("Maybe words that will get you out of the room.")
	add_hint("There's a possible, slim, tiny chance that 'exit room' would work.")
	_instructions()
	
func _instructions():
	write("You are in a room.")
	write("It is completely bare, with only a single exit.")
	write("You see on the ground the remains of your keyboard. All but two keys gone:")
	write("    The ENTER and ESC keys.")
	write("Your mouse, however, is perfectly fine.")
	pre_prompt()
	
func _post_confirm(result):
	if result == 'n':
		write("Okay! Feel free to pester me any time you want!")
		write()
	
func _user_input(input):
	var clean_input = input.replace("the ", "")
	match clean_input:
		"bare room":
			write("Completely.")
		"enter room":
			write("You're already in the room!?")
		"'exit room'":
			write("Enough with the attitude, thanks.")
		"help you":
			write("I think it's YOU that needs to be helped...")
		"exit room", "esc room":
			write("Correct! Moving on to the next level...")
			$CorrectTimer.start()
		"enter exit":
			write("...You're technically correct, I guess...")
			$CorrectTimer.start()
		"what":
			write("What?")
		"hint":
			show_hint()
		"instructions":
			_instructions()
		"quit":
			write("Quit to title screen? (y/n)")
			enter_confirm_mode("_quit")
		_:
			write("I'm not sure I understand.")
			write()
	
func _correct():
	SceneManager.next_level()