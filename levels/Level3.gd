extends "res://engine/HintLevel.gd"

var hole_exists = false
var round_number = 1

func _ready():
	$Shell.set_header(ASCII.generate_level_header("LEVEL THREE", false))
	add_hint("Whilst eating the banana may seem helpful, you'd better save it.")
	add_hint("It may be of use to you tomorrow.")
	add_hint("Care to join me for a picnic? I'm just outside the r...oh.")
	_instructions()

func _instructions():
	write("You are in a completely round room made of stone.")
	write("There's no doors, no windows. No exits at all.")
	write("Not even a hint of adventure for our hero.")
	write()
	if hole_exists:
		write("Except, maybe, that massive gaping hole in the wall.")
	else:
		write("The only object in this room is a banana, of all things.")
		write("Maybe you can eat it later. It's broken into two halves, though.")
	pre_prompt()
	
func _post_confirm(result):
	if result == 'n':
		round_number += 1
		write("Round %s, let's go!" % round_number)
		if round_number == 10:
			if hole_exists:
				write("You know this is meant to be the EASY part, right?")
			else:
				write("You're either very persistent or extremely indecisive.")
		write()

func _user_input(input):
	var clean_input = input.replace("the ", "")
	match clean_input:
		"join banana", "join banana halves", "join halves":
			if hole_exists:
				write("You join the two halves of the bana- woah.")
				write("...")
				write("D\u00E9j\u00E0 vu.")
			else:
				write("You join the two halves of the banana together.")
				write("The two halves form to make a hole- -wait, that's a typo!-")
				write("Before I can correct my error, you throw the banana at the wall.")
				write()
				write("There is now a gaping hole in the wall. Considerably more helpful.")
				write("You take a moment to celebrate how much your situation has improved.")
				write()
				hole_exists = true
				round_number = 1
				write("[updated instructions]")
		"join hole":
			write("And just for a moment, you and the hole join in perfect harmony, and you exist as one.")
		"join you":
			write("Unfortunately the walls of this room are in your way. It was rather mean of me, I must admit.")
		"eat things":
			write("What things?")
		"eat all things":
			write("All you have to eat is this banana. And it's far too symmetrical for a meager snack.")
		"eat banana":
			if hole_exists:
				write("There's not much left of it, I'm afraid.")
			else:
				write("You attempt to eat the banana, but the banana is complaining of a splitting headache.")
		"eat banana tomorrow":
			if hole_exists:
				write("You can't even eat the banana TODAY.")
			else:
				write("I'm not even sure if we'll be ready to eat this majestic segmented treat next *year*!")
		"eat banana next year", "eat banana later":
			write("You probably will.")
		"eat it":
			write("Ah, eat 'it'. Very well.")
			write("...")
			write("You eat it. What next?")
		"eat hole":
			write("Interesting concept. Unfortunately, 'tis a banana no more.")
		"exist as one":
			write("Only once, my friend. Only once.")
		"use banana", "use banana now":
			if hole_exists:
				write("Uh, yeah. You already did.")
			else:
				write("...for what?") 
				write("You can't eat it. That would be absurd.")
		"use banana tomorrow", "use banana later":
			write("Okay.")
		"use banana next year":
			write("Next year is, in fact, the year of the banana.")
		"escape":
			write("How?")
		"escape hole tomorrow", "use hole tomorrow":
			write("It would be far more effective to do that today.")
		"escape hole next year", "use hole next year":
			write("Ah, taking one of your famous year-long naps?")
		"escape hole", "use hole":
			_win_sequence()
		"escape room":
			if hole_exists:
				_win_sequence()
			else:
				write("If it were only that easy...")
		"object":
			write("Overruled.")
		"save banana":
			if hole_exists:
				write("I'm sorry. It's too late. The banana cannot be saved.")
			else:
				write("In this desolate room, you decide on the perfect place to save a banana:")
				write("Right where it is!")
		"save it":
			write("Save what?")
		"save you":
			write("Unfortunately, you were unable to escape from the room in time.")
			write("I have been eaten by the dragon and replaced by a new narrator.")
			write("One who is far less tolerant of stupid actions.")
		"what":
			write("What?")
		"hint":
			show_hint()
		"instructions", "updated instructions":
			_instructions()
		"quit":
			if hole_exists:
				write("Hole: 1, Player: 0? (y/n)")
			else:
				write("Room: 1, Player: 0? (y/n)")
			enter_confirm_mode("_quit")
		_:
			write("I'm not sure I understand.")
			write()

func _correct():
	SceneManager.change_scene("res://levels/Credits.tscn")
	
func _win_sequence():
	write("You climb through the hole.")
	write()
	write("Unfortunately, a picnic is nowhere to be found.")
	write("Never mind, carrying on!")
	$CorrectTimer.start()
	