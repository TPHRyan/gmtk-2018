extends Timer

func _ready():
	connect("timeout", self, "_timeout")

func _timeout():
	SceneManager.exit()