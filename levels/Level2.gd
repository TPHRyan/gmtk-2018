extends "res://engine/HintLevel.gd"

func _ready():
	$Shell.set_header(ASCII.generate_level_header("LEVEL TWO"))
	add_hint("Nice people who deserve help don't start fires.")
	add_hint("I'm thinking that I probably shouldn't assist a sociopath.")
	add_hint("Nope. I'm out.")
	_instructions()
	
func _instructions():
	write("You are alone in a burning building.")
	write("As the structure collapses, you hear all around you the terrifying screams of your enemies.")
	write()
	write("Your enemies?")
	write()
	write("I'm beginning to get the picture that it was YOU that started the fire.")
	pre_prompt()
	
func _post_confirm(result):
	if result == 'n':
		write("Of course you're not.")
		write()
	
func _user_input(input):
	var clean_input = input.replace("the ", "")
	match clean_input:
		"assist enemies":
			write("They're all either dead or screaming. You monster.")
		"assist sociopath":
			write("Try you must, but one cannot pull themselves up by their bootstraps.")
		"don't start fires":
			write("Yeah. Okay.")
			write("You don't start a fire.")
			write("There's only the MASSIVE EXISTING FIRE to worry about now.")
		"get picture":
			write("It's only a metaphorical picture! You can't actually touch it...") 
		"hear enemies":
			write("It's pretty hard not to at this stage")
		"i'm a sociopath":
			write("Haha, cute. Are you going to win the level already?")
		"start fires":
			write("It was starting fires that got you INTO this mess, George.")
		"get out":
			write("Oops. That's ironic. Moving on...")
			$CorrectTimer.start()
		"what":
			write("What?")
		"hint":
			show_hint()
		"instructions":
			_instructions()
		"quit":
			write("Are you done murdering innocents? (y/n)")
			enter_confirm_mode("_quit")
		_:
			write("I'm not sure I understand.")
			write()
	
func _correct():
	SceneManager.next_level()