extends "res://engine/Level.gd"

var _in_esc_confirm_mode = false

func _ready():
	$ClearTimer.connect("timeout", self, "_clear_feedback")
	$Shell.set_header_line1(" */==========================================================================\\* ")
	$Shell.set_header_line2(" ||                        /// ESCAPE THE KEYBOARD ///                       || ")
	$Shell.set_header_line3(" *\\==========================================================================/* ")
	_draw_title_screen()
	
func _physics_process(delta):
	if Input.is_action_just_pressed("broken_esc"):
		if _in_esc_confirm_mode:
			_quit()
		else:
			_clear_feedback()
			$Shell/TerminalDisplay.write("Press ESC again to quit. (In the real game, your keyboard is *broken*.)", Constants.CONSOLE_HEIGHT - 2, 2)
			_in_esc_confirm_mode = true
	
func _clear_feedback():
	if not _in_confirm_mode and not _in_esc_confirm_mode:
		$Shell/TerminalDisplay.clear_line(Constants.CONSOLE_HEIGHT - 2)
	
func _correct():
	SceneManager.new_game()

func _draw_title_screen():
	write(" |                                                                            | ")
	write(" |                                                                            | ")
	write(" |                               [ START GAME ]                               | ")
	write(" |                                                                            | ")
	write(" |                               [  QUIT GAME ]                               | ")
	write(" |                                                                            | ")
	write(" |                                                                            | ")
	write(" |        Left mouse button = Type          Right mouse button = Enter        | ")
	write(" |                                                                            | ")
	write(" *\\==========================================================================/* ")
	
func _post_confirm(result):
	_clear_feedback()
	
func _text_contains_feedback_words(text):
	text = text + ' ' # Add space for searching whole words
	return (text.find("all ") > -1) \
	or (text.find("already ") > -1) \
	or (text.find("but ") > -1) \
	or (text.find("done ") > -1) \
	or (text.find("exactly ") > -1) \
	or (text.find("foot ") > -1) \
	or (text.find("gave ") > -1) \
	or (text.find("idea ") > -1) \
	or (text.find("keen ") > -1) \
	or (text.find("little ") > -1) \
	or (text.find("name ") > -1) \
	or (text.find("nonsense ") > -1) \
	or (text.find("not ") > -1) \
	or (text.find("off ") > -1) \
	or (text.find("required ") > -1) \
	or (text.find("sentence ") > -1) \
	or (text.find("starting ") > -1) \
	or (text.find("starts ") > -1) \
	or (text.find("that's ") > -1) \
	or (text.find("too ") > -1) \
	or (text.find("until ") > -1) \
	or (text.find("wait ") > -1) \
	or (text.find("whatever ") > -1) \
	or (text.find("whole ") > -1) \
	or (text.find("writing ") > -1) \
	or (text.find("you're ") > -1)
	
func _user_input(input):
	if (_in_esc_confirm_mode):
		_in_esc_confirm_mode = false
		_clear_feedback()
	match input:
		"enter":
			_write_titlescreen_feedback("One of the many keys not required to play this game!...It still works, though.")
		"enter game":
			_write_titlescreen_feedback("...I GAVE you the whole sentence. All of it! Whatever, starting game...")
			start_correct_timer()
		"escape":
			_write_titlescreen_feedback("There's no escaping now.")
		"escape keyboard":
			_write_titlescreen_feedback("Already done! No keyboard required.")
		"escape the keyboard":
			_write_titlescreen_feedback("That's the name of the game!")
		"start", "start game":
			_write_titlescreen_feedback("Starting game, here we go...")
			start_correct_timer()
		"type":
			_write_titlescreen_feedback("Typing is literally the one thing you cannot do. Well, that and long division.")
		"quit", "quit game":
			_clear_feedback()
			$Shell/TerminalDisplay.write("Are you sure you want to quit? (y/n)", Constants.CONSOLE_HEIGHT - 2, 2)
			enter_confirm_mode("_quit")
		_:
			if _text_contains_feedback_words(input):
				_write_titlescreen_feedback("Right idea, but you're a little too keen. Wait until the game starts.")
			else:
				_write_titlescreen_feedback("Writing nonsense? You're not exactly starting off on the right foot.")
			
func _write_titlescreen_feedback(text):
	_clear_feedback()
	$Shell/TerminalDisplay.write(text, Constants.CONSOLE_HEIGHT - 2, 2)
	$ClearTimer.start()