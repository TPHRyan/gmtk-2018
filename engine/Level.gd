extends Node

var _confirm_action = null
var _in_confirm_mode = false

func _ready():
	$CorrectTimer.connect("timeout", self, "_correct")
	$Shell.connect("user_input", self, "_check_confirm")
		
func enter_confirm_mode(confirm_action):
	_confirm_action = confirm_action
	_in_confirm_mode = true
		
func write(text=""):
	$Shell.text_writeln(text)
	
func pre_prompt():
	write("What do you do?")
	write()
	
func start_correct_timer():
	$CorrectTimer.start()
	$Shell.disable()
	
func _check_confirm(input):
	if _in_confirm_mode:
		if input == 'y':
			if _confirm_action != null:
				var conf_act = _confirm_action
				_confirm_action = null
				call_deferred(conf_act)
			_in_confirm_mode = false
			_post_confirm(input)
		elif input == 'n':
			_in_confirm_mode = false
			_post_confirm(input)
	else:
		_user_input(input)
	
func _correct():
	pass
	
func _post_confirm(result):
	pass
	
func _quit():
	SceneManager.exit()
	
func _user_input(input):
	pass