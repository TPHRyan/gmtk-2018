extends Node

const HEADER_HEIGHT = 3

var disabled = false
var header_line1 = ""
var header_line2 = ""
var header_line3 = ""
var text_lines = []
var prompt_content = ""

var MAX_PROMPT_WIDTH = Constants.CONSOLE_WIDTH - Constants.CONSOLE_LEFT_MARGIN - 2
var MAX_TEXT_HEIGHT = Constants.CONSOLE_HEIGHT - Constants.CONSOLE_TOP_MARGIN - HEADER_HEIGHT - 1 # 1 for the prompt area

signal user_input

func _ready():
	$TerminalDisplay.clear()
	prompt_clear()
	text_clear()
	
	var blank_header = ""
	for i in range(0, 80):
		blank_header += '\u2591'
	header_line1 = blank_header
	header_line2 = blank_header
	header_line3 = blank_header
		
	$TerminalDisplay.connect("word_clicked", self, "_word_clicked")
	
func _physics_process(delta):
	if not disabled and Input.is_action_just_pressed("dos_enter"):
		if prompt_content == "":
			return
		emit_signal("user_input", prompt_content.strip_edges())
		prompt_clear()
		
func disable():
	disabled = true
	
func enable():
	disabled = false
	
func prompt_clear():
	prompt_content = ""
	_draw_prompt()
	
func prompt_write(word):
	prompt_content += word
	_draw_prompt()
	
func set_header(new_header):
	set_header_line2(new_header)

func set_header_line1(new_header):
	header_line1 = new_header
	_draw_header()
	
func set_header_line2(new_header):
	header_line2 = new_header
	_draw_header()
	
func set_header_line3(new_header):
	header_line3 = new_header
	_draw_header()
	
func text_clear():
	text_lines = []
	_draw_text()
	
func text_writeln(line=""):
	_add_line(line)
	_draw_text()
	
func _add_line(line):
	var line_max_width = Constants.CONSOLE_WIDTH - Constants.CONSOLE_LEFT_MARGIN
	var line_width = line.length()
	if line_width <= line_max_width:
		text_lines.push_back(line)
	else:
		var first_line
		while line_width > line_max_width:
			if line.substr(line_max_width - 1, 1) == ' ' or line.substr(line_max_width, 1) == ' ':
				first_line = line.substr(0, line_max_width)
				line = line.substr(line_max_width, line_width - line_max_width)		
			else:
				first_line = line.substr(0, line_max_width - 1) + '-'
				line = line.substr(line_max_width - 1, line_width - line_max_width + 1)
			line_width = line.length()
			text_lines.push_back(first_line)
		text_lines.push_back(line)	
	while text_lines.size() > MAX_TEXT_HEIGHT:
		text_lines.pop_front()
		
func _clear_header():
	for y in range(Constants.CONSOLE_TOP_MARGIN, Constants.CONSOLE_TOP_MARGIN + HEADER_HEIGHT):
		for x in range(0, Constants.CONSOLE_WIDTH):
			$TerminalDisplay.write(' ', y, x)
		
func _draw_header():
	_clear_header()
	$TerminalDisplay.set_draw_cursor()
	$TerminalDisplay.writeln(header_line1)
	$TerminalDisplay.writeln(header_line2)
	$TerminalDisplay.writeln(header_line3)

func _clear_prompt():
	for x in range(0, Constants.CONSOLE_WIDTH):
		$TerminalDisplay.write(' ', Constants.CONSOLE_HEIGHT - 1, x)
	
func _draw_prompt():
	_clear_prompt()
	$TerminalDisplay.write('>', Constants.CONSOLE_HEIGHT - 1, Constants.CONSOLE_LEFT_MARGIN)
	
	var draw_content = prompt_content
	if draw_content.length() + 1 > MAX_PROMPT_WIDTH: # +1 to allow for cursor
		draw_content = draw_content.substr(draw_content.length() - MAX_PROMPT_WIDTH + 1, MAX_PROMPT_WIDTH - 1) # +1 and -1 to allow for cursor
	$TerminalDisplay.write(draw_content, Constants.CONSOLE_HEIGHT - 1, Constants.CONSOLE_LEFT_MARGIN + 2)
	
	$TerminalDisplay.set_cursor(Constants.CONSOLE_HEIGHT - 1, Constants.CONSOLE_LEFT_MARGIN + draw_content.length() + 2)
	
func _clear_text():
	for y in range(
		Constants.CONSOLE_TOP_MARGIN + HEADER_HEIGHT, 
		Constants.CONSOLE_TOP_MARGIN + HEADER_HEIGHT + MAX_TEXT_HEIGHT
	):
		for x in range(0, Constants.CONSOLE_WIDTH):
			$TerminalDisplay.write(' ', y, x)
	
func _draw_text():
	_clear_text()
	$TerminalDisplay.set_draw_cursor(Constants.CONSOLE_TOP_MARGIN + HEADER_HEIGHT, Constants.CONSOLE_LEFT_MARGIN)
	for line in text_lines:
		$TerminalDisplay.writeln(line)
	
func _word_clicked(word, begin_word, end_word):
	if not disabled:
		prompt_write(word.to_lower() + ' ')