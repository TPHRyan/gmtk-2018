extends Node

var Character = preload("res://engine/Character.tscn")
var cursor = Vector2(-1, -1)
var cursor_blink_state = Constants.UNDERSCORE
var cursor_char = null
var cursor_char_code = Constants.UNDERSCORE
var cursor_underneath = -1
var draw_cursor = Vector2()
var screen = []

signal char_clicked
signal word_clicked

func _ready():
	set_draw_cursor()
	$CursorTimer.connect("timeout", self, "_cursor_blink")
	
	var x_margin = (Constants.screen_width - (Constants.CONSOLE_WIDTH * Constants.CHAR_WIDTH)) / 2
	var y_margin = (Constants.screen_height - (Constants.CONSOLE_HEIGHT * Constants.CHAR_HEIGHT)) / 2
	for y in range(0, Constants.CONSOLE_HEIGHT):
		var row = []
		for x in range(0, Constants.CONSOLE_WIDTH):
			var character_instance = Character.instance()
			character_instance.set_name(str(x) + "," + str(y))
			character_instance.set_character(Constants.BLANK)
			character_instance.position = Vector2(x * Constants.CHAR_WIDTH + x_margin, y * Constants.CHAR_HEIGHT + y_margin)
			character_instance.connect("char_clicked", self, "_char_clicked")
			row.append(character_instance)
			add_child(character_instance)
		screen.append(row)
			
func clear():
	fill(Constants.BLANK)
	set_draw_cursor()
	
func clear_line(y=0):
	fill_line(Constants.BLANK, y)
	
func fill_line(char_code, y=0):
	for x in range(0, Constants.CONSOLE_WIDTH):
		screen[y][x].set_character(char_code)
			
func fill(char_code):
	for y in range(0, Constants.CONSOLE_HEIGHT):
		for x in range(0, Constants.CONSOLE_WIDTH):
			screen[y][x].set_character(char_code)
	
func put_char(chr, y=0, x=0):
	_put_char(ASCII.CHARACTER_MAP[chr], y, x)
	
func set_cursor(y=-1, x=-1):
	$CursorTimer.stop()
	if cursor.x >= 0 and cursor.y >= 0 and cursor_underneath >= 0:
		screen[cursor.y][cursor.x].set_character(cursor_underneath)
	cursor = Vector2(x, y)
	if cursor.x >= 0 and cursor.y >= 0:
		cursor_char = screen[cursor.y][cursor.x]
		cursor_underneath = cursor_char.get_character()
		screen[cursor.y][cursor.x].set_character(cursor_char_code)
		$CursorTimer.start()
		
func set_draw_cursor(y=Constants.CONSOLE_TOP_MARGIN, x=Constants.CONSOLE_LEFT_MARGIN):
	draw_cursor = Vector2(x, y)	
	
func write(to_write, y=0, x=0):
	var current_x = x
	var current_y = y
			
	for c in to_write:
		# Guard to make x and y are within bounds
		if current_x >= Constants.CONSOLE_WIDTH:
			current_x = Constants.CONSOLE_LEFT_MARGIN
			current_y += 1
			if current_y >= Constants.CONSOLE_HEIGHT:
				break
		if cursor == Vector2(current_x, current_y):
			cursor_underneath = ASCII.CHARACTER_MAP[c]
		else:
			_put_char(ASCII.CHARACTER_MAP[c], current_y, current_x)
		current_x += 1
	self.draw_cursor = Vector2(current_x, current_y)
	
func writeln(to_write):
	if self.draw_cursor.x > Constants.CONSOLE_LEFT_MARGIN:
		_next_line()
	self.write(to_write, draw_cursor.y, draw_cursor.x)
	_next_line()
	
func _calculate_word(char_position):
	var current_character = screen[char_position.y][char_position.x].get_character()
	if ASCII.is_punctuation(current_character):
		return
	var word = [current_character]
	var left = _calculate_word_left(char_position)
	var right = _calculate_word_right(char_position)
	var word_begin = left[0]
	var word_end = right[0]
	word = left[1] + word + right[1]
	emit_signal("word_clicked", _translate_word(word), word_begin, word_end)
	
func _calculate_word_left(char_position):
	if char_position.x == 0:
		return [char_position, []]
	var left_character = screen[char_position.y][char_position.x - 1].get_character()
	if ASCII.is_punctuation(left_character):
		return [char_position, []]
	var left = _calculate_word_left(Vector2(char_position.x - 1, char_position.y))
	return [left[0], left[1] + [left_character]]

func _calculate_word_right(char_position):
	if char_position.x >= Constants.CONSOLE_WIDTH - 1:
		return [char_position, []]
	var right_character = screen[char_position.y][char_position.x + 1].get_character()
	if right_character == Constants.HYPHEN and char_position.y < Constants.CONSOLE_HEIGHT - 1:
		return _calculate_word_right(Vector2(Constants.CONSOLE_LEFT_MARGIN - 1, char_position.y + 1))
	if ASCII.is_punctuation(right_character):
		return [char_position, []]
	var right = _calculate_word_right(Vector2(char_position.x + 1, char_position.y))
	return [right[0], [right_character] + right[1]]
	
func _char_clicked(char_position):
	var clicked_char = screen[char_position.y][char_position.x]
	emit_signal("char_clicked", ASCII.STRING_MAP[clicked_char.get_character()], char_position)
	_calculate_word(char_position)
	
func _cursor_blink():
	match cursor_blink_state:
		Constants.BLANK:
			cursor_blink_state = cursor_char_code
		_:
			cursor_blink_state = Constants.BLANK
	cursor_char.set_character(cursor_blink_state)
	
func _next_line():
	draw_cursor = Vector2(Constants.CONSOLE_LEFT_MARGIN, draw_cursor.y + 1)
	
func _put_char(char_code, y=0, x=0):
	screen[y][x].set_character(char_code)
	
func _translate_word(word):
	var result = ""
	for c in word:
		result += ASCII.STRING_MAP[c]
	return result