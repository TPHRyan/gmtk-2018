extends Node

var game_node = null
var current_level = 1
var scene_stack = []
var next_level_resource = null

func _ready():
	var root = get_tree().root
	game_node = root.find_node("SceneRoot", true, false)
	if game_node == null:
		game_node = root
		scene_stack.push_back(preload("res://TitleScreen.tscn"))
	scene_stack.push_back(preload("res://TitleScreen.tscn"))

func change_scene(path):
	# Defer so all currently running code can be run
	call_deferred("_deferred_change_scene", path)
	
func _deferred_change_scene(path):
	_switch_scene(ResourceLoader.load(path))
	
func exit():
	# Defer so all currently running code can be run
	call_deferred("_deferred_exit")
	
func _deferred_exit():
	if (scene_stack.size() > 1):
		_pop_scene()
	else:
		get_tree().quit()
	

func new_game():
	# Defer so all currently running code can be run
	call_deferred("_deferred_new_game")
	
func _deferred_new_game():
	current_level = 1
	_push_scene(ResourceLoader.load(Constants.LEVELS[0]))
	_load_next_level()
	
func next_level():
	# Defer so all currently running code can be run
	call_deferred("_deferred_next_level")
	
func _deferred_next_level():
	if next_level_resource == null:
		_load_next_level()
	current_level += 1
	_switch_scene(next_level_resource)
	_load_next_level()
	
func _pop_scene():
	# Remove the current scene
	var current_scene = _get_current_scene()
	game_node.remove_child(current_scene)
	current_scene.free()
	var popped_scene_resource = scene_stack.pop_back()
	
	if not scene_stack.empty():
		# Add back the previous scene
		game_node.add_child(_get_current_scene_resource().instance())
	
	return popped_scene_resource
		
func _push_scene(scene_resource):
	# Stop playing the current scene
	game_node.remove_child(_get_current_scene())
	
	# Add in the new scene
	var next_scene = scene_resource.instance()
	game_node.add_child(next_scene)
	scene_stack.push_back(scene_resource)

func _switch_scene(scene_resource):
	# Replace the current scene with the new one
	var current_scene = _get_current_scene()
	game_node.remove_child(current_scene)
	current_scene.free()
	game_node.add_child(scene_resource.instance())
	
	# Switch the scenes on the stack and return the popped resource
	var popped_scene_resource = scene_stack.pop_back()
	scene_stack.push_back(scene_resource)
	return popped_scene_resource
		
func _get_current_scene():
	return game_node.get_child(game_node.get_child_count() - 1)
	
func _get_current_scene_resource():
	return scene_stack.back()

func _load_next_level():
	# Load the next level so it's ready when this one finishes
	if Constants.LEVELS.size() > current_level:
		next_level_resource = ResourceLoader.load(Constants.LEVELS[current_level])