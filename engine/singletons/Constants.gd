extends Node

const CHAR_WIDTH = 9
const CHAR_HEIGHT = 16

const CONSOLE_WIDTH = 80
const CONSOLE_HEIGHT = 25
const CONSOLE_LEFT_MARGIN = 0
const CONSOLE_TOP_MARGIN = 0

const BLANK = 0
const HYPHEN = 45
const UNDERSCORE = 95

const LEVELS = [
	"res://levels/Level1.tscn",
	"res://levels/Level2.tscn",
	"res://levels/Level3.tscn",
]

var screen_width = ProjectSettings.get_setting("display/window/size/width")
var screen_height = ProjectSettings.get_setting("display/window/size/height")