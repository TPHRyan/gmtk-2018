extends "res://engine/Level.gd"

var hints = []
var hint_count = 0

func _ready():
	pass
	
func add_hint(hint_text):
	hints.push_back(hint_text)
	
func show_hint():
	write(("HINT #%s: " % (hint_count + 1)) + hints[hint_count])
	pre_prompt()
	hint_count = (hint_count + 1) % hints.size()