extends Area2D

signal char_clicked

func _ready():
	pass
	
func get_character():
	return $Sprite.frame
	
func set_character(code):
	$Sprite.frame = code
	
func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
	and event.button_index == BUTTON_LEFT \
	and event.is_pressed():
		emit_signal("char_clicked", _char_position())
		
func _char_position():
	var x_and_y = name.split(",")
	return Vector2(int(x_and_y[0]), int(x_and_y[1]))